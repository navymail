#ifndef _NAVYMAIL_MBOX_WALK_H
#define _NAVYMAIL_MBOX_WALK_H

/*
 * Copyright (C) 2012  Kirill Smelkov <kirr@navytux.spb.ru>
 *
 * This program is free software: you can Use, Study, Modify and Redistribute it
 * under the terms of the GNU General Public License version 2. This program is
 * distributed WITHOUT ANY WARRANTY. See COPYING file for full License Terms.
 */


#include "git-compat-util.h"
#include "strbuf.h"
#include <stdio.h>

struct mbox_walk {
	FILE *fmbox;

	/* current message & line */
	struct strbuf  msg, line;

	/* will skip junk till the first From_ */
	unsigned seen_from_ : 1;
};


void prepare_mbox_walk(struct mbox_walk *, FILE *fmbox);
int /*entry_ok*/ next_mbox_entry(struct mbox_walk *);

#endif

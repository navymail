#ifndef _NAVYMAIL_GBOX_WALK_H
#define _NAVYMAIL_GBOX_WALK_H

/*
 * Copyright (C) 2011  Kirill Smelkov <kirr@navytux.spb.ru>
 *
 * This program is free software: you can Use, Study, Modify and Redistribute it
 * under the terms of the GNU General Public License version 2. This program is
 * distributed WITHOUT ANY WARRANTY. See COPYING file for full License Terms.
 */

#include "git-compat-util.h"
#include "cache.h"
#include "commit.h"
#include "diff.h"
#include "object.h"
#include "revision.h"
#include "sha1-array.h"

struct gbox_walk {
	/* refname of gbox to walk */
	const char *gbox;
	/* walk gbox in original order (default is reverse chronological order) */
	unsigned original_order : 1;


	/* iter over revisions */
	struct rev_info revs;

	/* current tree and iter over it */
	struct tree *tree;
	struct tree_desc tree_desc;

	/* for original_order, preloaded tree entries and current idx */
	struct sha1_array tree_entries;
	int entry_idx;
};


void prepare_gbox_walk(struct gbox_walk *);
const unsigned char * /*sha1*/ next_gbox_entry(struct gbox_walk *);

#endif

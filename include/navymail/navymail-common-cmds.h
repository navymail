/* TODO Automatically generate */
struct cmdname_help {
	char name[16];
	char help[80];
};


static struct cmdname_help navymail_common_cmds[] = {
  {"import",	"Import mbox into navymail store"},
  {"init",	"Initialize navymail store"},
  {"export",	"Export mbox from navymail store"},
  {"mount",	"Mount filesystem representing navymail store"},
};

#ifndef _NAVYMAIL_H
#define _NAVYMAIL_H

/*
 * Copyright (C) 2011  Kirill Smelkov <kirr@navytux.spb.ru>
 *
 * This program is free software: you can Use, Study, Modify and Redistribute it
 * under the terms of the GNU General Public License version 2. This program is
 * distributed WITHOUT ANY WARRANTY. See COPYING file for full License Terms.
 */


#define	NAVYMAIL_DIR_ENVIRONMENT	"NAVYMAIL_DIR"
#define NAVYMAIL_EXEC_PATH_ENV		"NAVYMAIL_EXEC_PATH"


/* for fromgit/builtin/help.c */
const char *setup_navymail_directory_gently(int *);

/* for fromgit/help.c & friends */
extern const char navymail_usage_string[];
extern const char navymail_more_info_string[];
extern const char navymail_version_string[];

/*
 * Prefix and exec-path are always detected at runtime.
 *
 * Also in git code using GIT_(MAN|INFO|HTML)_PATH is patched to
 * navymail_\1_path() for paths to be determined at runtime.
 */
const char *navymail_prefix();
void navymail_set_argv_exec_path(const char *exec_path);
const char *navymail_exec_path();
const char *navymail_man_path();
const char *navymail_info_path();
const char *navymail_html_path();

/*
 * builtins
 * XXX we don't need prefix - it has meaning only for worktree
 */
extern int cmd_export	(int argc, const char **argv, const char *prefix);
extern int cmd_import	(int argc, const char **argv, const char *prefix);
extern int cmd_mount	(int argc, const char **argv, const char *prefix);

#endif

/* walk through gbox
 * Copyright (C) 2011  Kirill Smelkov <kirr@navytux.spb.ru>
 *
 * This program is free software: you can Use, Study, Modify and Redistribute it
 * under the terms of the GNU General Public License version 2. This program is
 * distributed WITHOUT ANY WARRANTY. See COPYING file for full License terms.
 */


#include "navymail/gbox-walk.h"
#include "tag.h"


/* TODO die -> error codes */
void prepare_gbox_walk(struct gbox_walk *w)
{
	/* TODO convert to argv_array */
	const char *rev_argv[3];
	int i, rev_argc=0;

	rev_argv[rev_argc++] = NULL;
	if (w->original_order)
		rev_argv[rev_argc++] = "--reverse";
	rev_argv[rev_argc++] = w->gbox;


	init_revisions(&w->revs, NULL/*prefix - ok?*/);
	if (setup_revisions(rev_argc, rev_argv, &w->revs, NULL) != 1)
		die("gbox-walk: setup revision failed");

	/* unmark revs.pending commits as seen, so gbox walking could be used
	 * several times (this mimics git/handle_commit() a bit
	 */
	for (i = 0; i < w->revs.pending.nr; ++i) {
		struct object *object = w->revs.pending.objects[i].item;

		/* tag -> tagee */
		while (object->type == OBJ_TAG) {
			struct tag *tag = (struct tag *) object;
			if (!tag->tagged)
				die("bad tag");
			object = parse_object(tag->tagged->sha1);
			if (!object)
				die("bad object %s", sha1_to_hex(tag->tagged->sha1));
		}

		if (object->type == OBJ_COMMIT)
			clear_commit_marks((struct commit *)object, SEEN | UNINTERESTING | SHOWN | ADDED);
	}


	if (prepare_revision_walk(&w->revs))
		die("gbox-walk: revision walk setup failed");

	w->tree = NULL;
	w->tree_entries.sha1 = NULL;	/* = SHA1_ARRAY_INIT */
	w->entry_idx = -1;
}


const unsigned char * /*sha1*/ next_gbox_entry(struct gbox_walk *w)
{
	struct name_entry entry;

start:
	/* see, if we need to fetch next commit */
	if (!w->tree) {
		struct commit *commit;

		commit = get_revision(&w->revs);
		if (!commit)
			return NULL;

		/* printf("%s\n", sha1_to_hex(commit->object.sha1)); */
		w->tree = commit->tree;
		if (parse_tree(w->tree))
			die("gbox-walk: parse_tree failed");

		init_tree_desc(&w->tree_desc, w->tree->buffer, w->tree->size);


		/* preload the tree for original order */
		if (w->original_order) {
			while (tree_entry(&w->tree_desc, &entry))
				sha1_array_append(&w->tree_entries, entry.sha1);

			w->entry_idx = w->tree_entries.nr - 1;
		}
	}


	/* fast case - walking gbox in reverse order */
	if (!w->original_order) {
		if (tree_entry(&w->tree_desc, &entry))
			return entry.sha1;
	}

	/* slow case - exporting mbox in original order */
	else {
		if (w->entry_idx >= 0)
			return w->tree_entries.sha1[w->entry_idx--];

		sha1_array_clear(&w->tree_entries);
	}


	/* restart from next commit */
	w->tree = NULL;
	goto start;
}

/* iterate messages in mbox
 * Copyright (C) 2012  Kirill Smelkov <kirr@navytux.spb.ru>
 *
 * This program is free software: you can Use, Study, Modify and Redistribute it
 * under the terms of the GNU General Public License version 2. This program is
 * distributed WITHOUT ANY WARRANTY. See COPYING file for full License terms.
 */


#include "navymail/mbox-walk.h"
#include "cache.h"


void prepare_mbox_walk(struct mbox_walk *m, FILE *fmbox)
{
	m->fmbox = fmbox;
	strbuf_init(&m->msg, 0);
	strbuf_init(&m->line, 0);
	m->seen_from_ = 0;
}


/* see if line is an mbox(5) "From " line */
static int is_from_line(struct strbuf *line)
{
	/*
	 * 1) "From "
	 * 2) envelope sender-addr	; addr-spec as per RFC 2822 3.4.1
	 * 3) whitespace
	 * 4) timestamp			; date-time as output by asctime(3)
	 *
	 * e.g.
	 *
	 *	From example@example.com Fri Jun 23 02:56:55 2000
	 */

	int envelope_ok, date_ok;
	const char *p, *q;

	/* "From " */
	if (strncmp(line->buf, "From ", 5))
		return 0;

	p = line->buf + 5;

	/* envelope */
	for (q=p; *q && !isspace(*q); ++q)
		;
	envelope_ok = (q!=p);	/* simply check whether it is present */

	/* whitespace */
	for (; isspace(*q); ++q)
		;

	/* date */
	date_ok = !parse_date_basic(q, /* timestamp,offset = */NULL,NULL);

	if (!envelope_ok || !date_ok)
		warning("mbox: Strange From_ line \"%s\"--%s%s\n",
				line->buf,
				!envelope_ok ? " !envelope_ok" : "",
				!date_ok     ? " !date_ok" : "");


	return 1;
}

int next_mbox_entry(struct mbox_walk *m)
{
	int eof, entry_ok;

	strbuf_reset(&m->msg);

	while (1) {
		strbuf_addbuf(&m->msg, &m->line);

		eof = strbuf_getwholeline(&m->line, m->fmbox, '\n');
		if (eof)
			break;

		if (is_from_line(&m->line)) {
			if (m->seen_from_)
				break;

			/* this was first From_ line - continue
			 * NOTE: we are skipping everything before first From_ !
			 */
			m->seen_from_ = 1;
		}
	}

	entry_ok = (m->msg.len != 0);
	if (!entry_ok) {
		/* free msg & line on stop */
		strbuf_release(&m->msg);
		strbuf_release(&m->line);
	}

	return entry_ok;
}

// grep for function/structure/define ...
// Usage: spatch -very_quiet -sp_file grep.cocci -D (func|structname|definename)=<IDENTIFIER> <FILE>
//
// Copyright (C) 2011  Kirill Smelkov <kirr@navytux.spb.ru>
//
// This program is free software: you can Use, Study, Modify and Redistribute it
// under the terms of the GNU General Public License version 2. This program is
// distributed WITHOUT ANY WARRANTY. See COPYING file for full License terms.

@ initialize:python @
_found = []	# [] of (file, bline,bcolumn, eline,ecolumn)

# register found text via begin/end positions
def found(pb, pe):
	b = pb[0]	# XXX more positions?
	e = pe[0]	# XXX more positions?

	assert b.file == e.file
	f = b.file
	bl= int(b.line)
	bc= int(b.column)
	el= int(e.line_end)
	ec= int(e.column_end)

	#print '%s %i:%i - %i:%i' % (f, bl,bc, el,ec)
	_found.append( (f, bl,bc, el,ec) )


// file processed -> emit found items
@ finalize:python @
#print 'fini'
import sys
if not _found:
	sys.exit(1)	# mimic grep exit status

files = {}	# filename -> [] of lines
for f, bl,bc, el,ec in _found:
	#print f,bl,bc,el,ec
	try:
		flines = files[f]
	except KeyError:
		fobj   = open(f, 'r')
		flines = fobj.readlines()
		files[f] = flines
		fobj.close()

	bl -= 1;	el -= 1-1;	# cocci index lines from 1, end inclusive; python from 0, end exclusive
	bc -= 0;	ec -= 0;	# cocci index columns from 0, end exclusive, like python does

	lines = flines[bl:el]
	lines[-1] = lines[-1][:ec]+'\n'
	lines[0]  = lines[0] [bc:]

	print ''.join(lines),


// XXX does not pick static from `static int func(...) ...`
// match whole function, extract positions
@ func @
identifier virtual.func;
type T;
position pb, pe;
@@

T @pb func (...) { ... } @pe

// ... and process matches with python
@ script:python @
pb << func.pb;
pe << func.pe;
@@
found(pb, pe)


// ---//--- struct
@ structdef @
identifier virtual.structname;
position pb, pe;
@@

struct @pb structname { ... }; @pe

@ script:python @
pb << structdef.pb;
pe << structdef.pe;
@@
found(pb, pe)


// ---//--- define
@ definedef @
identifier virtual.definename;
expression E;
position pb, pe;
@@

// FIXME move @pb earlier
#define definename @pb E @pe

@ script:python @
pb << definedef.pb;
pe << definedef.pe;
@@
found(pb, pe)

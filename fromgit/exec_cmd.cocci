// Tweak git/exec-cmd.c for navymail needs
// Copyright (C) 2011  Kirill Smelkov <kirr@navytux.spb.ru>
//
// This program is free software: you can Use, Study, Modify and Redistribute it
// under the terms of the GNU General Public License version 2. This program is
// distributed WITHOUT ANY WARRANTY. See COPYING file for full License terms.

@@
@@
 #include "..."
+#include "navymail/navymail.h"


// system_path: always use runtime prefix, navymail way
@@
@@
 system_path(const char *path)
 {
-  ...	// kill init for static const char prefix under #ifdef
+  const char *prefix = navymail_prefix();

   struct strbuf d = STRBUF_INIT;

   if (is_absolute_path(path))
     return path;

-  ...	// kill git's approach to runtime prefix under #ifdef

   strbuf_addf(&d, "%s/%s", prefix, path);
   ...
 }

// git's set/get exec_path -> navymail
@@
@@
-git_set_argv_exec_path
+navymail_set_argv_exec_path
 (const char *exec_path)
 {
   ...
   setenv(
-         EXEC_PATH_ENVIRONMENT,
+         NAVYMAIL_EXEC_PATH_ENV,
          exec_path, 1);
 }

@@
@@
-git_exec_path
+navymail_exec_path
 (void)
 {
   ...
   env = getenv(
-         EXEC_PATH_ENVIRONMENT
+         NAVYMAIL_EXEC_PATH_ENV
   );
   ...
-  return system_path(GIT_EXEC_PATH);
   // XXX how to use NAVYMAIL_EXEC_PATH_ENV here?
+  /* argv_exec_path should be set early by navymail_setup_path() */
+  die("BUG: both argv_exec_path and $NAVYMAIL_EXEC_PATH unset.");
 }



/*
 * XXX at present "git" is changed to "navymail" in prepare_git_cmd() and
 * execv_git_cmd(). I'm not sure this is not correct, because we might want to
 * change those functions to navymail_ later and run git via our own special
 * helper. Postponing this issue.
 */


// include "common.cocci"  (XXX "included" by build system via generating %.cocci+)

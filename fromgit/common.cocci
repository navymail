// Common tweaks for git/* for navymail needs
// Copyright (C) 2011  Kirill Smelkov <kirr@navytux.spb.ru>
//
// This program is free software: you can Use, Study, Modify and Redistribute it
// under the terms of the GNU General Public License version 2. This program is
// distributed WITHOUT ANY WARRANTY. See COPYING file for full License terms.

// git -> navymail for *_string
@@ @@
- git_usage_string
+ navymail_usage_string

@@ @@
- git_more_info_string
+ navymail_more_info_string

@@ @@
- git_version_string
+ navymail_version_string


// replace git's common_cmds with navymail ones
@@ @@
-#include "common-cmds.h"
+#include "navymail/navymail-common-cmds.h"

@@ @@
-common_cmds
+navymail_common_cmds


// git's exec_path -> navymail
@@ @@
-git_exec_path
+navymail_exec_path

@@ @@
-git_set_argv_exec_path
+navymail_set_argv_exec_path


// GIT_*_PATH -> navymail_*_path()
@@ @@
-GIT_MAN_PATH
+navymail_man_path()

@@ @@
-GIT_INFO_PATH
+navymail_info_path()

@@ @@
-GIT_HTML_PATH
+navymail_html_path()


// setup_git -> setup_navymail
@@ @@
-setup_git_directory
+setup_navymail_directory

@@ @@
-setup_git_directory_gently
+setup_navymail_directory_gently


// git -> navymail in "" strings
@ cstr @
constant char [] msg;
@@
 msg

@ script:python reword @
msg << cstr.msg;
msg2;
@@
import re
#print `msg`, `msg.expr`
t = msg.expr
t = re.sub('git', 'navymail', t)
t = re.sub('Git', 'Navymail', t)
if t == msg.expr:
    # skip unchanged strings to avoid reformatting-only changes
    #print 'Skip: %r' % t
    cocci.include_match(False)
coccinelle.msg2 = t

@@
constant char [] cstr.msg;
identifier reword.msg2;
@@
-msg
+msg2

// Tweak git/git.c for navymail needs
// Copyright (C) 2011  Kirill Smelkov <kirr@navytux.spb.ru>
//
// This program is free software: you can Use, Study, Modify and Redistribute it
// under the terms of the GNU General Public License version 2. This program is
// distributed WITHOUT ANY WARRANTY. See COPYING file for full License terms.

// we include everything ourselves
@@
@@
-#include "..."

// drop version -- we provide our own
@@
@@
- const char git_version_string[];



@@
@@
 const char
-           git_more_info_string
+           navymail_more_info_string
 [];	// NOTE text will be tweaked by general git -> navymail text converter

// XXX is this pure-renaming needed at all?
// git_startup_info -> navymail_startup_info
// XXX why simple '-git_* +navymail_*' does not work?
@@
@@
 struct startup_info
-                    git_startup_info;
+                    navymail_startup_info;

@@
@@
- git_startup_info
+ navymail_startup_info


// tweak handle_options
// XXX how not to put all if-else in here?
// NOTE neg_if iso disabled, becuase otherwise it is *very* slow (i.e. 35s vs 4s with disabled neg_if)
@ disable neg_if @
statement S;
@@
 handle_options(...)
 {
   ...
   while (*argc > 0) {
     ...
     if (!prefixcmp(cmd, "--exec-path")) { ... }	// ok	NOTE git_*exec_path() -> navymail_ is done by common code
     else if (!strcmp(cmd, "--html-path")) { ... }	// ok
     else if (!strcmp(cmd, "--man-path")) { ... }	// ok
     else if (!strcmp(cmd, "--info-path")) { ... }	// ok
     else if (!strcmp(cmd, "-p") || !strcmp(cmd, "--paginate")) { ... }	// ok
     else if (!strcmp(cmd, "--no-pager")) { ... }	// ok
-    else if (!strcmp(cmd, "--no-replace-objects")) { ... }	// not needed
     else if (!strcmp(cmd,				// --git-dir  ->  --navymail-dir
-                         "--git-dir"
+                         "--navymail-dir"
     )) {
       ...
       setenv(
-             GIT_DIR_ENVIRONMENT,
+             NAVYMAIL_DIR_ENVIRONMENT,
              (*argv)[1], 1);
       ...
     }
     else if (!prefixcmp(cmd,				// --git-dir=  ->  --navymail-dir=
-                            "--git-dir="
+                            "--navymail-dir="
     )) {
       setenv(
-             GIT_DIR_ENVIRONMENT,
+             NAVYMAIL_DIR_ENVIRONMENT,
              cmd +
-                  10,
+                  15,
              1);
       ...
     }
-    else if (!strcmp(cmd, "--namespace")) { ... }	// not needed
-    else if (!prefixcmp(cmd, "--namespace=")) { ... }	// not needed
-    else if (!strcmp(cmd, "--work-tree")) { ... }	// not needed
-    else if (!prefixcmp(cmd, "--work-tree=")) { ... }	// not needed
-    else if (!strcmp(cmd, "--bare")) { ... }		// not needed - always bare
     else S
     ...
   }
   ...
 }

// ... and remove just-killed options from usage
// (NOTE git -> navymail options rename will be done by common code)
@ usage @
constant char [] msg;
@@
  const char
-            git_usage_string
+            navymail_usage_string
  [] = msg;

@ script:python reword_usage @
msg << usage.msg;
msg2;
@@
import re
t = msg.expr
t = re.sub('\[--no-replace-objects\]', '', t)
t = re.sub('\[--namespace=<name>\]', '', t)
t = re.sub('\[--work-tree=<path>\]', '', t)
t = re.sub('\[--bare\]', '', t)
t = re.sub(r' +\\n"', r'\\n"', t) # strip trailing whitespace after options removal
coccinelle.msg2 = t

@@
constant char [] usage.msg;
identifier reword_usage.msg2;
@@
-msg
+msg2


// we do not support aliases -- fake them out
@@
@@
 handle_alias(...)
 {
-  ...
+  /* aliases support removed */
+  return 0;
 }


// replace commands[] from inside handle_internal_command with navymail ones
// XXX better specify our commands in C code
@@
@@
 handle_internal_command(...)
 {
   ...
   static struct cmd_struct commands[] = {
-	...
+	{ "export",	cmd_export,	RUN_SETUP },
+	{ "help",	cmd_help },
+	{ "import",	cmd_import,	RUN_SETUP },
+	{ "mount",	cmd_mount,	RUN_SETUP },
+	{ "version",	cmd_version },
   };
   ...
 }


@@
@@
 main(...)
 {
   ...
+  navymail_setup_path();
   cmd = git_extract_argv0_path(argv[0]);
   ...
   if (!prefixcmp(cmd,
-	"git-"
+	"navymail-"
   )) {
 	cmd +=
-		4;
+		9;
	...
   }
   ...
 }


// include "common.cocci"  (XXX "included" by build system via generating %.cocci+)

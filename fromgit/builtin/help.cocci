// Tweak git/builtin/help.c for navymail needs
// Copyright (C) 2011  Kirill Smelkov <kirr@navytux.spb.ru>
//
// This program is free software: you can Use, Study, Modify and Redistribute it
// under the terms of the GNU General Public License version 2. This program is
// distributed WITHOUT ANY WARRANTY. See COPYING file for full License terms.

@@
@@
 #include "..."
+#include "navymail/navymail.h"

// // XXX this is just cosmetics - do we need it?
// // XXX how to combine this 2 rules into 1?
// @@
// @@
// -is_git_command
// +is_navymail_command
//  (...) { ... }
// @@
// @@
// -is_git_command
// +is_navymail_command


// include "common.cocci"  (XXX "included" by build system via generating %.cocci+)

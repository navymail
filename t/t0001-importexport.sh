#!/bin/sh
# Copyright (C) 2011 Kirill Smelkov <kirr@navytux.spb.ru>

test_description='Test mbox import/export'
. ./test-lib.sh


D="$TEST_DIRECTORY/t0001"

test_expect_success 'importing mbox in one go' '
	navymail import sup-talk $D/sup-talk.mbox,1-17 &&
	navymail export --original-order sup-talk > sup-talk.original &&
	test_cmp $D/sup-talk.mbox,1-17 sup-talk.original &&
	navymail export sup-talk > sup-talk.reverse &&
	test_cmp $D/sup-talk.mbox,17-1 sup-talk.reverse
'

test_expect_success 'importing mbox incrementally' '
	navymail import sup-talk2 $D/sup-talk.mbox,1-2 &&
	navymail import sup-talk2 $D/sup-talk.mbox,3-4 &&
	navymail import sup-talk2 $D/sup-talk.mbox,5-17 &&
	navymail export --original-order sup-talk2 > sup-talk2.original &&
	test_cmp $D/sup-talk.mbox,1-17 sup-talk2.original &&
	navymail export sup-talk2 > sup-talk2.reverse &&
	test_cmp $D/sup-talk.mbox,17-1 sup-talk2.reverse
'


# at present, there is no way to filter out duplicate messages from several imports
test_expect_failure "importing duplicate mbox'es" '
	navymail import dup $D/sup-talk.mbox,1-2 &&
	navymail import dup $D/sup-talk.mbox,1-2 &&
	navymail export --original-order dup > dup.original &&
	test_cmp $D/sup-talk.mbox,1-2 dup.original
'


test_done

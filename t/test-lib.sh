# Test harness for navymail based on Git's t/test-lib.sh
# Copyright (C) 2011 Kirill Smelkov <kirr@navytux.spb.ru>
#
# This program is free software: you can Use, Study, Modify and Redistribute it
# under the terms of the GNU General Public License version 2, or any later
# version. This program is distributed WITHOUT ANY WARRANTY. See COPYING file
# for full License terms.

git=$(cd ../git && pwd)		# Git is located here
objtree=$(cd .. && pwd)		# navymail's objtree
t=$(pwd)			# our test directory

# so that's git's test-lib.sh don't try to verify git has been built
GIT_TEST_INSTALLED=$(dirname $(which git))

# temporarily set TEST_DIRECTORY to git place, so that git's test-lib.sh would
# correctly set GIT_BUILD_DIR, etc...
TEST_DIRECTORY=$git/t

# we also need to explicitly tweak root, so that git's test-lib.sh don't set
# TRASH_DIRECTORY and descendants to inside $git
root=$t # TODO still allow $root to be overwritten by --root

# XXX is it possible to omit test_create_repo() running by git's test-lib?
. $git/t/test-lib.sh

# reset TEST_DIRECTORY back to us
TEST_DIRECTORY=$t


# --- now navymail part ---

export PATH="$objtree:$PATH"

unset NAVYMAIL_DIR

# Most tests can use the created mailstore, but some may need to create more.
# Usage: test_create_mailstore <dir>
test_create_mailstore() {
	test "$#" = 1 ||
	error "bug in the test script: not 1 parameter to test_create_mailstore"
	navymail init "$1" >&3 2>&4 ||
		error "cannot run 'navymail init' -- have you built things yet?"
}


mailstore="$test/test.gbox"
test_create_mailstore "$mailstore"
export NAVYMAIL_DIR="$mailstore"

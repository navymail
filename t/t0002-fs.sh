#!/bin/sh
# Copyright (C) 2011 Kirill Smelkov <kirr@navytux.spb.ru>

test_description='Test filesystem interface'
. ./test-lib.sh


D="$TEST_DIRECTORY/t0001"

# XXX partially dup from t0001-importexport.sh
test_expect_success 'import test mail' '
	navymail import sup-talk	$D/sup-talk.mbox,1-2 &&
	navymail import sup-talk	$D/sup-talk.mbox,3-4 &&
	navymail import sup-talk	$D/sup-talk.mbox,5-17 &&
	navymail import sup-talk2	$D/sup-talk.mbox,1-17 &&
	navymail import x/zzz		$D/sup-talk.mbox,1-2 &&
	navymail import x/yyy		$D/sup-talk.mbox,3-4 &&
	navymail import x/y/qqq		$D/sup-talk.mbox,5-17 &&
	navymail import x/y/ddd		$D/sup-talk.mbox,17-1
'

navymail_mountpath=
navymail_mount() {
	trap 'code=$?; navymail_umount; (exit $code); die' EXIT
	navymail_mountpath="$(pwd)/$1"
	navymail mount "$navymail_mountpath"
}

navymail_umount() {
	test -n "$navymail_mountpath" || return

	fusermount -u "$navymail_mountpath"
	navymail_mountpath=
}


test_expect_success 'mount it' '
	mkdir fs &&
	>expect &&
	ls -F fs >actual &&
	test_cmp expect actual &&
	navymail_mount fs
'

test_expect_success 'basic filesystem layout' '
	echo "mail/" >expect &&
	ls -F fs >actual &&
	test_cmp expect actual &&
	cat >expect <<EOF &&
sup-talk
sup-talk2
x/
EOF
	ls -F fs/mail >actual &&
	test_cmp expect actual &&
	cat >expect <<EOF &&
y/
yyy
zzz
EOF
	ls -F fs/mail/x >actual &&
	test_cmp expect actual &&
	cat >expect <<EOF &&
ddd
qqq
EOF
	ls -F fs/mail/x/y >actual &&
	test_cmp expect actual
'

test_expect_success 'read file content' '
	cat fs/mail/sup-talk >read1 &&
	cat fs/mail/sup-talk >read2 &&
	cat fs/mail/sup-talk >read3 &&
	test_cmp read1 read2 &&
	test_cmp read2 read3 &&
	navymail export --original-order sup-talk >sup-talk.mbox &&
	cat fs/mail/sup-talk >sup-talk.read &&
	test_cmp sup-talk.mbox sup-talk.read &&
	test_cmp sup-talk.read read1 &&
	test_cmp fs/mail/sup-talk2	$D/sup-talk.mbox,1-17 &&
	test_cmp fs/mail/x/zzz		$D/sup-talk.mbox,1-2 &&
	test_cmp fs/mail/x/yyy		$D/sup-talk.mbox,3-4 &&
	test_cmp fs/mail/x/y/qqq	$D/sup-talk.mbox,5-17 &&
	test_cmp fs/mail/x/y/ddd	$D/sup-talk.mbox,17-1
'

cat >expect <<EOF
Date: Wed, 31 Oct 2007 09:42:58 +0100
Date: Mon, 29 Oct 2007 16:40:37 -0500
Date: Mon, 29 Oct 2007 14:43:07 -0700
Message-ID: <1193694144-sup-4506@south>
Subject: [ANN] Sup 0.2 released
Received: from localhost.localdomain
Sender: sup-talk-bounces@rubyforge.org
- Add custom code to handle certain types of messages or to handle
Date: Mon, 29 Oct 2007 18:55:59 -0700
Date: Tue, 30 Oct 2007 15:55:18 +0000
rrrr
Date: Tue, 30 Oct 2007 15:55:18 +0000
        from /Users/brendano/sw/sup/lib/sup/index.rb:323:in \`load_entry_for_id'
EOF

# tests already-seen msg lookup in navymailfs_read
test_expect_success 'read file content seeky' '
	$TEST_DIRECTORY/xdd	\
		53843,38	\
		  115,38	\
		 5118,38	\
		 5156,40	\
		 5086,32	\
		 6084,37	\
		 7053,39	\
		 8017,67	\
		 8862,38	\
		23731,38	\
		 4994,1		\
		 8709,1		\
		13374,1		\
		15883,1		\
		15926,1		\
		23731,38	\
		12221,80	\
	<fs/mail/sup-talk >actual &&
	test_cmp expect actual
'



navymail_umount
test_done

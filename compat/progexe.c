#include "navymail/compat/progexe.h"
#include "git-compat-util.h"

/*
 * Determine path to program executable
 * (based on example from http://stackoverflow.com/questions/2050961)
 */
const char *xprogexe()
{
	static char progexe_path[PATH_MAX+1];

#if defined(__linux__)
	ssize_t len = readlink("/proc/self/exe", progexe_path, PATH_MAX);
	if (len==-1)
		die_errno("progexe: Cannot readlink /proc/self/exe");

	progexe_path[len] = '\0';

#elif defined(_WIN32)
	/* XXX not tested */
	DWORD len = GetModuleFileNameA(NULL, progexe_path, PATH_MAX);
	if (!len)
		/* TODO use GetLastError here */
		die("progexe: GetModuleFileNameA(NULL) failed");

#elif defined(__APPLE__) /* elif of: #elif defined(__linux__) */
	/* XXX not tested, TODO cleanup */
#include <mach-o/dyld.h>
	uint32_t pathNameSize = 0;

	_NSGetExecutablePath(NULL, &pathNameSize);

	if (pathNameSize > pathNameCapacity)
		pathNameSize = pathNameCapacity;

	if (!_NSGetExecutablePath(pathName, &pathNameSize))
	{
		char real[PATH_MAX];

		if (realpath(pathName, real) != NULL)
		{
			pathNameSize = strlen(real);
			strncpy(pathName, real, pathNameSize);
		}
		return pathNameSize;
	}

	die("progexe: fail on apple");	/* XXX error code ? */

#else
	#error xprogexe: provide your own implementation
#endif

	return progexe_path;
}

/* import mbox into navymail store
 * Copyright (C) 2011-2012  Kirill Smelkov <kirr@navytux.spb.ru>
 *
 * This program is free software: you can Use, Study, Modify and Redistribute it
 * under the terms of the GNU General Public License version 2. This program is
 * distributed WITHOUT ANY WARRANTY. See COPYING file for full License terms.
 */

#include "git-compat-util.h"
#include "parse-options.h"
#include "cache.h"
#include "object.h"
#include "blob.h"
#include "tree.h"
#include "commit.h"
#include "refs.h"
#include "sha1-array.h"
#include "run-command.h"
#include "navymail/navymail.h"
#include "navymail/mbox-walk.h"

/* XXX better naming for gbox */
static const char * const import_usage[] = {
	"navymail import <gbox> <mbox>",
	NULL
};

static const struct option import_options[] = {
	OPT_END()
};


static const char *argv_git_gc_auto[] = { "git", "gc", "--auto", NULL };


static int import_mbox(const char *gbox, FILE *fmbox)
{
	struct mbox_walk m;

	unsigned char blob_sha1[20];
	struct sha1_array blobs = SHA1_ARRAY_INIT;

	struct strbuf tree_buf = STRBUF_INIT;
	unsigned char tree_sha1[20];

	unsigned char parent_sha1[20], commit_sha1[20];
	struct commit *parent = NULL;
	struct commit_list *parents = NULL;
	struct strbuf commit_msg = STRBUF_INIT;
	struct strbuf reflog_msg = STRBUF_INIT;

	int i, nmsg_width, nmsg, err;


	/* (1) msgs -> blobs */
	prepare_mbox_walk(&m, fmbox);

	while (next_mbox_entry(&m)) {
		err = write_sha1_file(m.msg.buf, m.msg.len, blob_type, blob_sha1);
		if (err)
			die("import: Unable to put msg -> blob");

		sha1_array_append(&blobs, blob_sha1);
	}

	/* (2) blobs -> tree	; in reverse order, so that more recent msg comes first
	 *
	 * NOTE	we have to be careful, and prepare tree entry names in the same
	 *      order as done git-mktree. Git expects tree entries to be sorted by
	 *      name, so we prepend enough leading 0 to make that sort numerical.
	 */
	nmsg_width = 0;
	for (i=blobs.nr; i>0; i /= 10)
		nmsg_width++;

	for (i=0; i<blobs.nr; ++i) {
		strbuf_addf(&tree_buf, "100644 %0*i%c", nmsg_width, i, '\0');
		strbuf_add (&tree_buf, blobs.sha1[blobs.nr-i-1], 20);
	}

	err = write_sha1_file(tree_buf.buf, tree_buf.len, tree_type, tree_sha1);
	if (err)
		die("import: Unable to put blobs -> tree");

	/* free tree and blobs[] early - they could be large for bulk imports */
	strbuf_release(&tree_buf);
	nmsg = blobs.nr;
	sha1_array_clear(&blobs);

	/* (3) tree -> commit */
	if (!get_sha1(gbox, parent_sha1)) {
		parent = lookup_commit_or_die(parent_sha1, gbox);
		err = parse_commit(parent);
		if (err)
			die("import: could not parse %s commit", gbox);
	}

	strbuf_addf(&commit_msg, "Import %i msg into %s", nmsg, gbox);
	strbuf_addf(&reflog_msg, "commit (mail%s)", parent ? "" : ", initial");

	if (parent)
		commit_list_insert(parent, &parents);

	err = commit_tree(&commit_msg, tree_sha1, parents, commit_sha1, /*author=*/NULL, /*sign=*/NULL);
	if (err)
		die("import: Unable to put tree -> commit");

	update_ref(reflog_msg.buf, gbox, commit_sha1,
			parent ? parent->object.sha1 : NULL, 0, DIE_ON_ERR);

	/* (4) houskeeping */
	run_command_v_opt(argv_git_gc_auto, 0);


	strbuf_release(&commit_msg);
	strbuf_release(&reflog_msg);
	return 0;
}

int cmd_import(int argc, const char **argv, const char *prefix)
{
	const char *gbox, *mbox;
	struct strbuf gbox_ref = STRBUF_INIT;
	FILE *fmbox;
	int ret;

	argc = parse_options(argc, argv, NULL /*prefix?*/, import_options, import_usage, 0);

	/* git_config(git_default_config, NULL); ? */

	if (argc != 2)
		usage_with_options(import_usage, import_options);

	gbox = argv[0];
	strbuf_addf(&gbox_ref, "refs/mail/%s", gbox);	/* XXX don't hardcode mail/ here */

	mbox = argv[1];
	if (!strcmp(mbox, "-"))
		fmbox = stdin;
	else {
		fmbox = fopen(mbox, "r");
		if (!fmbox)
			die_errno("Can't open %s", mbox);
	}

	ret = import_mbox(gbox_ref.buf, fmbox);

	if (fmbox != stdin)
		fclose(fmbox);

	strbuf_release(&gbox_ref);
	return ret;
}

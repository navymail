/*
 * Navymail - a program to store and synchronize mail in Git.
 * Copyright (C) 2011 Kirill Smelkov <kirr@navytux.spb.ru>
 *
 * This program is free software: you can Use, Study, Modify and Redistribute it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * See COPYING file for full License terms.
 *
 * --------
 *
 * Main driver for executing commands.
 *
 */

#include "builtin.h"
#include "cache.h"
#include "exec_cmd.h"
#include "help.h"
#include "git-compat-util.h"
#include "run-command.h"

#include "navymail/navymail.h"
#include "navymail/compat/progexe.h"


/* TODO move out-of-here + autogenerate. XXX dup in configure.ac */
const char navymail_version_string[] = "0.001";



/* Setup libexec and other paths for navymail at runtime */
static const char *prefix, *man_path, *info_path, *html_path;

const char *navymail_prefix()		{ return prefix; }
const char *navymail_man_path()		{ return man_path; }
const char *navymail_info_path()	{ return info_path; }
const char *navymail_html_path()	{ return html_path; }

void navymail_setup_path()
{
	char *exe;
	const char *exec_path;

	/*
	 * This simulates runtime prefix.
	 */
	exe = xstrdup(xprogexe());


	/*
	 * TODO also support installed case
	 */
	prefix = xstrdup(dirname(exe));		/* '/path/to/navymail'  -> '/path/to' */


	/* NOTE setting this early, still allows exec-path to be overriden via
	 * --exec-path */
	exec_path = xstrdup(mkpath("%s/cmd", prefix));
	if (!getenv(NAVYMAIL_EXEC_PATH_ENV))
		navymail_set_argv_exec_path(exec_path);

	/* XXX man,info,html are stubs - i.e. they are not used atm */
	man_path  = xstrdup(mkpath("%s/Documentation/man", prefix));
	info_path = xstrdup(mkpath("%s/Documentation/info", prefix));
	html_path = xstrdup(mkpath("%s/Documentation/html", prefix));

	free(exe);
}



const char *setup_navymail_directory_gently(int *non_navymail)
{
	const char *navymail_dir;

	/* require navymail-dir to be explicitly set */
	navymail_dir = getenv(NAVYMAIL_DIR_ENVIRONMENT);
	if (!navymail_dir) {
		if (non_navymail)
			*non_navymail = 1;
		return NULL;
	}


	/* propagate navymail-dir to git-dir */
	setenv(GIT_DIR_ENVIRONMENT, navymail_dir, 1);

	/* always --bare */
	is_bare_repository_cfg = 1;

	return setup_git_directory_gently(non_navymail);
}


const char *setup_navymail_directory()
{
	const char *prefix;
	int non_navymail;

	prefix = setup_navymail_directory_gently(&non_navymail);
	if (non_navymail) {
		if (!getenv(NAVYMAIL_DIR_ENVIRONMENT))
			die("What is your navymail-dir?");
		else
			die("Repo setup failed");
	}

	return prefix;
}



/* the rest of the code comes from tweaked git/git.c */
#include "fromgit/git.c"

/* export mbox from navymail store
 * Copyright (C) 2011  Kirill Smelkov <kirr@navytux.spb.ru>
 *
 * This program is free software: you can Use, Study, Modify and Redistribute it
 * under the terms of the GNU General Public License version 2. This program is
 * distributed WITHOUT ANY WARRANTY. See COPYING file for full License terms.
 *
 *
 * NOTE	by default we export messages in reverse chronological order, because it
 *	is faster, and because it is what people are usually interested in.
 *
 *	messages can be exported in original order with --original-order.
 */

#include "git-compat-util.h"
#include "parse-options.h"
#include "strbuf.h"
#include "navymail/navymail.h"
#include "navymail/gbox-walk.h"

/* XXX better naming for gbox */
static const char * const  export_usage[] = {
	"navymail export [--original-order] <gbox>",
	NULL
};

static int original_order;

static const struct option export_options[] = {
	OPT_BOOLEAN( 0, "original-order", &original_order, "export messages in original order"),
	OPT_END()
};


static void cat_blob(const unsigned char *blob_sha1, const unsigned char *tree_sha1)
{
	enum object_type type;
	void *buf;
	unsigned long size;

	buf = read_sha1_file(blob_sha1, &type, &size);
	if (!buf)
		die("navymail export: corrupt tree %s", sha1_to_hex(tree_sha1));
	if (type != OBJ_BLOB)
		die("navymail export: corrupt tree %s (got %s instead of %s)",
			sha1_to_hex(tree_sha1),
			typename(type), typename(OBJ_BLOB));

	write_or_die(1, buf, size);
	free(buf);
}

static int export_gbox(const char *gbox)
{
	struct gbox_walk gbox_walk = { .gbox = gbox, .original_order = original_order };
	const unsigned char *sha1;

	prepare_gbox_walk(&gbox_walk);

	while ((sha1 = next_gbox_entry(&gbox_walk)))
		cat_blob(sha1, gbox_walk.tree->object.sha1);

	return 0;
}


int cmd_export(int argc, const char **argv, const char *prefix)
{
	const char *gbox;
	struct strbuf gbox_ref = STRBUF_INIT;
	int ret;

	argc = parse_options(argc, argv, NULL /*prefix?*/, export_options, export_usage, 0);

	/* git_config(git_default_config, NULL); ? */

	if (argc != 1)
		usage_with_options(export_usage, export_options);

	gbox = argv[0];
	strbuf_addf(&gbox_ref, "refs/mail/%s", gbox);	/* XXX don't hardcode mail/ here */
	ret = export_gbox(gbox_ref.buf);	/* XXX or pass strbuf directly? */
	strbuf_release(&gbox_ref);
	return ret;
}

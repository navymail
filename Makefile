# make script to build navymail
# Copyright (C) 2011  Kirill Smelkov <kirr@navytux.spb.ru>
all	:

SPATCH	:= spatch

# don't delete intermediate targets
.SECONDARY:


# >>> decide early, whether we are doing a clean-style target
__cleaning := $(filter clean mrproper,$(MAKECMDGOALS))
ifneq ($(__cleaning),)
  # if cleaning, ensure there is no other targets besides clean
  ifneq ($(filter-out $(__cleaning),$(MAKECMDGOALS)),)
    $(error E: target(s) "$(filter-out $(__cleaning),$(MAKECMDGOALS))" \
               conflict with "$(__cleaning)")
  endif
endif


# >>> for pretty build output
ifeq ($(V),)
Q	:= @
Qsubdir	:= --no-print-directory
Qspatch := -very_quiet

QAUTOCONF	= @echo '   ' AUTOCONF $@;
QCONFIGURE	= @echo '   ' CONFIGURE...;
QRECONFIGURE	= @echo '   ' RECONFIGURE...;
QCONFACTIVATE	= @echo '   ' GEN $@ '(+ friends)';

QARtweak	= @echo '   ' ARtweak $@;
QSPATCH		= @echo '   ' COCCI $@;
QCLEAN		= @echo '   ' CLEAN;
QMRPROPER	= @echo '   ' MRPROPER;
endif

# cocci: don't show diff until requested
ifeq ($(VCOCCI),)
Qspatch	+= -no_show_diff
else
# drop -very_quiet on VCOCCI=1 and automatically show diff
Qspatch :=
endif



# >>> before building anything, load configure findings, reconfiguring if needed

# NOTE configure dependencies have to be listed here explicitely
configure: configure.ac scripts/mk/pkg.m4
	$(QAUTOCONF)autoconf --warnings=all
config.status: configure
	$(if $(wildcard $@),\
		$(QRECONFIGURE)./config.status --recheck,\
		$(QCONFIGURE)./configure)

ifeq ($(__cleaning),)
# rebuild .config.mk.autogen without initial warning
ifeq ($(wildcard .config.mk.autogen),)
  $(shell touch -t 197001020101 .config.mk.autogen)
endif
include .config.mk.autogen
.config.mk.autogen : config.status .config.mk.in
	$(QCONFACTIVATE)./config.status
endif

# local build tweaks could go here
-include .config.mk



# >>> then, before building navymail, (re-)build Git and import build variables from its build system

ifeq ($(__cleaning),)
  git-targets := libgit.a
  # test depends on git targets, without which git's test-lib.sh won't run
  # TODO test-chmtime -> test-chmtime$X
  git-targets += GIT-BUILD-OPTIONS test-chmtime

  # also test needs git/templates/blt which is put directly into recipe

  # reconfigure goes first
  .config.mk.git: | .config.mk.autogen
endif

# XXX unfortunately .PHONY makefiles are not re-read, so we have to do dirty
# tricks with MAKE_RESTARTS instead -- see bugs.debian.org/614916
# .PHONY: .config.mk.git
ifeq ($(MAKE_RESTARTS),)
  # touch trick is needed to tell make to rebuild it without initial warning
  # reset mtime only on first make pass
  $(shell touch -t 197001020101 .config.mk.git)
  .config.mk.git	: .	# mtime of anything should be after epoch
endif

.config.mk.git:
	$(Q)$(MAKE) $(Qsubdir) -C git/ -f ../scripts/mk/gitmk-proxy.mk $(git-targets) __export __export-file=../$@
ifeq ($(__cleaning),)
	$(Q)$(MAKE) $(Qsubdir) -C git/templates
endif

include .config.mk.git



# >>> now let's build navymail

CPPFLAGS:= -Igit/ -Iinclude/
CC	:= $(GIT_CC)
CFLAGS	:= $(GIT_ALL_CFLAGS) $(FUSE_CFLAGS)
# lib.a -> git/lib.a , but -lsomething stays the same
GIT_LIBS:= $(foreach _,$(GIT_LIBS),$(if $(filter -%,$_),$_,git/$_))
X	:= $(GIT_X)
QUIET_CC = $(GIT_QUIET_CC)



all	: navymail$X

# libgit.a is tweaked a bit with here-compiled code
GIT_LIBS_wo_libgit := $(filter-out git/libgit.a,$(GIT_LIBS))
fromgit/libgit.a: git/libgit.a fromgit/help.o fromgit/exec_cmd.o
	$(QARtweak)Atmp=`mktemp $@.XXXXXX.a`	&&\
	cp $< $$Atmp	&&\
	ar r $$Atmp $(wordlist 2,$(words $+),$+)	&&\
	mv -f $$Atmp $@

navymail$X: navymail.o export.o gbox-walk.o fs.o mbox-walk.o import.o fromgit/builtin/help.o compat/progexe.o fromgit/libgit.a
	$(QUIET_CC)$(CC) $(CFLAGS) -o $@ $^ $(GIT_LDFLAGS) $(GIT_LIBS_wo_libgit) $(FUSE_LIBS)

# TODO automatically extract dependencies on compile
%.o	: %.c
	$(QUIET_CC)$(CC) $(CFLAGS) $(CPPFLAGS) -c -o $@ $<


navymail.o : fromgit/git.c

# NOTE -no_includes so that it does not modify .h
# XXX -no_loops so that it does not take forever...
# XXX .cocci files can't include other .cocci (only .iso) so we do "includes"
#     manually via generating .cocci+
fromgit/%.c : git/%.c fromgit/%.cocci fromgit/common.cocci
	$(Q)cat fromgit/$*.cocci fromgit/common.cocci > fromgit/$*.cocci+
	$(QSPATCH) $(SPATCH) $(Qspatch) -no_loops -no_includes -sp_file fromgit/$*.cocci+ -o $@ $<


clean	:
	$(QCLEAN)$(RM)	\
		navymail	\
		fromgit/*.cocci+ fromgit/builtin/*.cocci+	\
		*.o fromgit/*.[coa] fromgit/builtin/*.[co] compat/*.o	\
		.config.mk.git

mrproper: clean
	$(QMRPROPER)$(RM) \
		configure config.status config.log .config.mk.autogen && \
		$(MAKE) -C git/ clean



test	: navymail$X
	$(Q)$(MAKE) $(Qsubdir) -C t/
